<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function gastosFijos()
    {
        return $this->hasMany('App\GastoFijo');
    }

    public function horasNoFacturables()
    {
        return $this->hasMany('App\HoraNoFacturada');
    }

    public function materiales()
    {
        return $this->hasMany('App\Material');
    }

    public function productos()
    {
        return $this->hasMany('App\Producto');
    }

    public function ventas()
    {
        return $this->hasMany('App\PrecioVenta');
    }
}
