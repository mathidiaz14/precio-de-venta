<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function productos()
    {
        return $this->belongsToMany('App\Producto', 'material_producto', 'material_id', 'producto_id')->withPivot('cantidad');
    }
}
