<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GastoFijo extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
