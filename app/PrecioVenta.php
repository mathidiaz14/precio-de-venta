<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrecioVenta extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function producto()
    {
        return $this->belongsTo('App\Producto');
    }
}
