<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        if(($request->contraseña != null) and ($request->confirmar_contraseña != null))
        {
            if($request->contraseña === $request->confirmar_contraseña)
            {
                $user->password = Hash::make($request->contraseña);
                $user->save();
            }else
            {
                Session(['alert_danger' => "Las contraseñas no coinciden"]);
                return back();
            }
        }

        $user->salario_neto     = $request->salario_neto;
        $user->horas_diarias    = $request->horas_diarias;
        $user->dias_año         = $request->dias_año;
        $user->fines_semana     = $request->fines_semana;
        $user->festivos         = $request->festivos;
        $user->vacaciones       = $request->vacaciones;
        $user->dias_baja        = $request->dias_baja;
        $user->save();

        Session(['alert_success' => "Los datos se modificaron correctamente"]);
                return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
