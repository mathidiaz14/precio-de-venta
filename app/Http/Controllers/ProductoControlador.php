<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use Auth;

class ProductoControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_de_dias_año =   Auth::user()->dias_año 
                    - Auth::user()->fines_semana 
                    - Auth::user()->festivos 
                    - Auth::user()->vacaciones 
                    - Auth::user()->dias_baja;             

        $total_de_dias_mes = $total_de_dias_año / 12;

        $total_gastos_fijos = 0; 
        foreach (Auth::user()->gastosFijos as $gf)
            $total_gastos_fijos += $gf->cantidad;            

        $total_horas_no_facturables = 0;
        foreach(Auth::user()->horasNoFacturables as $hnf)
            $total_horas_no_facturables += $hnf->horas;

        $total_horas_no_facturables_mes = $total_horas_no_facturables * 4.33;
        
        $total_horas_facturables_mes = ($total_de_dias_mes * Auth::user()->horas_diarias) - $total_horas_no_facturables_mes;

        $costo_vacaciones = (Auth::user()->salario_neto + $total_gastos_fijos) * Auth::user()->vacaciones / 365;

        $objetivo_de_facturacion = Auth::user()->salario_neto + $total_gastos_fijos + $costo_vacaciones;

        $precio_hora_objetivo = $objetivo_de_facturacion / $total_horas_facturables_mes;
        
        return view('productos', compact('precio_hora_objetivo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = new Producto();
        $producto->titulo = $request->titulo;
        $producto->horas_trabajo = $request->horas_trabajo;
        $producto->paquete = $request->paquete;
        $producto->impuestos = $request->impuestos;
        $producto->user_id = Auth::user()->id;
        $producto->save();

        foreach (Auth::user()->materiales as $material) {
            if($request['material_'.$material->id] != 0)
                $material->productos()->attach($producto->id, ['cantidad' => $request['material_'.$material->id]]);
        }

        Session(["alert_success" => "El producto se creo correctamente"]);

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = Producto::find($id);
        $producto->titulo = $request->titulo;
        $producto->horas_trabajo = $request->horas_trabajo;
        $producto->paquete = $request->paquete;
        $producto->impuestos = $request->impuestos;
        $producto->save();

        $producto->materiales()->detach();

        foreach (Auth::user()->materiales as $material) 
        {
            if($request['material_'.$material->id] != 0)
                $material->productos()->attach($producto->id, ['cantidad' => $request['material_'.$material->id]]);
        }

        Session(["alert_success" => "El producto se modifico correctamente"]);

        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = Producto::find($id);
        $producto->materiales()->detach();
        $producto->delete();

        Session(["alert_danger" => "El producto se elimino correctamente"]);
        return back();
    }
}
