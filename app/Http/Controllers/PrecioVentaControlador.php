<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PrecioVenta;
use Auth;

class PrecioVentaControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_de_dias_año =   Auth::user()->dias_año 
                    - Auth::user()->fines_semana 
                    - Auth::user()->festivos 
                    - Auth::user()->vacaciones 
                    - Auth::user()->dias_baja;             

        $total_de_dias_mes = $total_de_dias_año / 12;

        $total_gastos_fijos = 0; 
        foreach (Auth::user()->gastosFijos as $gf)
            $total_gastos_fijos += $gf->cantidad;            

        $total_horas_no_facturables = 0;
        foreach(Auth::user()->horasNoFacturables as $hnf)
            $total_horas_no_facturables += $hnf->horas;

        $total_horas_no_facturables_mes = $total_horas_no_facturables * 4.33;
        
        $total_horas_facturables_mes = ($total_de_dias_mes * Auth::user()->horas_diarias) - $total_horas_no_facturables_mes;

        $costo_vacaciones = (Auth::user()->salario_neto + $total_gastos_fijos) * Auth::user()->vacaciones / 365;

        $objetivo_de_facturacion = Auth::user()->salario_neto + $total_gastos_fijos + $costo_vacaciones;

        $precio_hora_objetivo = $objetivo_de_facturacion / $total_horas_facturables_mes;
        
        return view('venta', compact('precio_hora_objetivo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $venta = new PrecioVenta();
        $venta->producto_id = $request->producto;
        $venta->margen = $request->margen;
        $venta->margen_tienda = $request->margen_tienda;
        $venta->pasarela_pago = $request->pasarela_pago;
        $venta->user_id = Auth::user()->id;
        $venta->save();

        Session(['alert_success' => "El item se creo correctamente"]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $venta = PrecioVenta::find($id);
        $venta->producto_id = $request->producto;
        $venta->margen = $request->margen;
        $venta->margen_tienda = $request->margen_tienda;
        $venta->pasarela_pago = $request->pasarela_pago;
        $venta->save();

        Session(['alert_success' => "El item se modifico correctamente"]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $venta = PrecioVenta::find($id);
        $venta->delete();

        Session(['alert_danger' => "El item se elimino correctamente"]);

        return back();

    }
}
