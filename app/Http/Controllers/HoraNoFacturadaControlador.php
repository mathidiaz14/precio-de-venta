<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HoraNoFacturada;
use Auth;

class HoraNoFacturadaControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hora = new HoraNoFacturada();
        $hora->user_id = Auth::user()->id;
        $hora->titulo = $request->titulo;
        $hora->horas = $request->horas;
        $hora->save();

        Session(['alert_success' => "La hora se guardo correctamente"]);
        return back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hora = HoraNoFacturada::find($id);
        $hora->titulo = $request->titulo;
        $hora->horas = $request->horas;
        $hora->save();

        Session(['alert_success' => "La hora se modifico correctamente"]);
        return back();  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hora = HoraNoFacturada::find($id);
        $hora->delete();

        Session(['alert_success' => "Se elimino la hora"]);
        return back();
    }
}
