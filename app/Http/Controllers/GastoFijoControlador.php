<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GastoFijo;
use Auth;

class GastoFijoControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gasto = new GastoFijo();
        $gasto->user_id = Auth::user()->id;
        $gasto->titulo = $request->titulo;
        $gasto->cantidad = $request->cantidad;
        $gasto->save();

        Session(['alert_success' => "El gasto se guardo correctamente"]);
        return back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gasto = GastoFijo::find($id);
        $gasto->titulo = $request->titulo;
        $gasto->cantidad = $request->cantidad;
        $gasto->save();

        Session(['alert_success' => "El gasto se modifico correctamente"]);
        return back();  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gasto = GastoFijo::find($id);
        $gasto->delete();

        Session(['alert_success' => "Se elimino el gasto"]);
        return back();
    }
}
