<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function materiales()
    {
        return $this->belongsToMany('App\Material',)->withPivot('cantidad');
    }

    public function ventas()
    {
        return $this->hasMany('App\PrecioVenta');
    }
}
