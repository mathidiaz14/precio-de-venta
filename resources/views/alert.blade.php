@if (session('alert_success'))
    <div class="col-sm-12">
	    <div class="alert alert-success" role="alert">
	        {{ session('alert_success') }}
	        {{ session()->forget('alert_success') }}
	    </div>
    </div>
@elseif (session('alert_danger'))
    <div class="col-sm-12">
    	<div class="alert alert-danger" role="alert">
	        {{ session('alert_danger') }}
	        {{ session()->forget('alert_danger') }}
	    </div>
    </div>
@endif