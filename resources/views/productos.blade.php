@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('alert')
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">Nuevo producto</div>
                <div class="card-body">
                   <div class="col-sm-12 ">
                        <form action="{{url('producto')}}" method="post" class="form-horizontal row">
                            @csrf
                            <div class="form-group col-sm-12 col-md-4 col-lg-2">
                            	<label for="">Titulo</label>
                                <input type="text" class="form-control col-sm-12" id="titulo" name="titulo" value="" placeholder="Titulo" required="">
                            </div>
                            <div class="form-group col-sm-12 col-md-4 col-lg-2">
                            	<label for="">Horas de trabajo</label>
                                <input type="text" class="form-control col-sm-12" id="horas" name="horas_trabajo" value="" placeholder="Horas de trabajo" required="">
                            </div>
                            <div class="form-group col-sm-12 col-md-4 col-lg-2">
                            	<label for="">Costo de paquete</label>
                                <input type="number" class="form-control col-sm-12" id="paquete" name="paquete" placeholder="Costo paquete" >
                            </div>
                            <div class="form-group col-sm-12 col-md-4 col-lg-2">
                            	<label for="">Impuestos %</label>
                                <input type="number" class="form-control col-sm-12" id="impuestos" name="impuestos" placeholder="Impuestos">
                            </div>
                            <div class="form-group col-sm-12 col-md-4 col-lg-2">
                                <br>
                                <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#mensajeAgregarMaterialesProducto">
                                  <i class="fa fa-plus"></i>
                                  Materiales
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="mensajeAgregarMaterialesProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                Agregar cantidad de materiales al producto
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group row">
                                                <label for="" class="col-sm-6"><b>Material</b></label>
                                                <label for="" class="col-sm-6"><b>Cantidad</b></label>
                                            </div>
                                            @foreach(Auth::user()->materiales as $material)
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-6">{{$material->titulo}}</label>
                                                    <input type="text" class="form-control col-sm-5" name="material_{{$material->id}}" value="0">
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-md-4 col-lg-2">
                            	<br>
                                <button type="submit" class="btn btn-info btn-block">
                                    <i class="fa fa-save"></i>
                                    Guardar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
		<div class="col-sm-12">
        	<br>
            <div class="card">
                <div class="card-header">Productos</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>Titulo</th>
                                <th>Horas trabajadas</th>
                                <th>Costo material</th>
                                <th>Costo paquete</th>
                                <th>Impuestos</th>
                                <th>Costo final producto</th>
                                <th>Editar</th>
                                <th>Eliminar</th>
                            </tr>
                            @foreach(Auth::user()->productos as $producto)
                                @php
                                    $precio_material = 0;
                                    foreach($producto->materiales as $material)
                                        $precio_material += $material->pivot->cantidad * ($material->precio / $material->cantidad);

                                    $precio_trabajo = $producto->horas_trabajo * $precio_hora_objetivo;
                                @endphp
                                <tr>
                                    <th>{{$producto->titulo}}</th>
                                    <th>{{$producto->horas_trabajo}} horas - $ {{round($precio_trabajo,2)}}</th>
                                    <th>$ {{round($precio_material, 2)}}</th>
                                    <th>$ {{$producto->paquete}}</th>
                                    <th>{{$producto->impuestos}}% - $ {{round((($precio_material + $precio_trabajo + $producto->paquete) * $producto->impuestos) / 100 ,2)}}</th>
                                    <th>
                                        $ {{round(($precio_material + $precio_trabajo + $producto->paquete) * ( "1.".$producto->impuestos) ,2)}}
                                    </th>
                                    <th>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mensajeEditarProducto{{$producto->id}}">
                                          <i class="fa fa-edit"></i>
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="mensajeEditarProducto{{$producto->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Editar item</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                               <form action="{{url('producto', $producto->id)}}" method="post" class="form-horizontal">
                                                    @csrf
                                                    @method('PATCH')
                                                    <div class="form-group row">
                                                        <label for="" class="col-sm-4">Titulo</label>
                                                        <input type="text" class="form-control col-sm-7" id="titulo" name="titulo" value="{{$producto->titulo}}" required="">
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="" class="col-sm-4">Horas de trabajo</label>
                                                        <input type="text" class="form-control col-sm-7" id="horas" name="horas_trabajo" value="{{$producto->horas_trabajo}}" required="">
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="" class="col-sm-4">Costo de paquete</label>
                                                        <input type="number" class="form-control col-sm-7" id="paquete" name="paquete" value="{{$producto->paquete}}" required="">
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="" class="col-sm-4">Impuestos %</label>
                                                        <input type="number" class="form-control col-sm-7" id="impuestos" name="impuestos" value="{{$producto->impuestos}}" required="">
                                                    </div>
                                                    <hr>
                                                    <label for="">Materiales</label>    
                                                    @foreach(Auth::user()->materiales as $material)
                                                        <div class="form-group row">
                                                            <label for="" class="col-sm-6">{{$material->titulo}}</label>
                                                            <input type="number" class="form-control col-sm-5" name="material_{{$material->id}}" value=@if($material->productos()->where('producto_id', $producto->id)->first() != null) "{{$material->productos()->where('producto_id', $producto->id)->first()->pivot->cantidad}}" @else "0" @endif>
                                                        </div>
                                                    @endforeach
                                                    <hr>
                                                    <div class="form-group text-right">
                                                        <button type="submit" class="btn btn-info">
                                                            <i class="fa fa-save"></i>
                                                            Guardar
                                                        </button>
                                                    </div>
                                                </form>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </th>
                                    <th>
                                        @include('eliminar', ['url' => url('producto', $producto->id), 'id' => $producto->id])
                                    </th>
                                </tr>
                            @endforeach
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
  
@endsection