<!-- Button trigger modal -->
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#mensajeEliminar{{$id}}">
  <i class="fa fa-trash"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="mensajeEliminar{{$id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <form action="{{$url}}" method="post">
            @csrf
            @method('DELETE')
            <i class="fa fa-exclamation-triangle fa-5x"></i>
            <h3>¿Desea eliminar el item?</h3>
            <hr>
            <div class="row">
              <div class="col">
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">NO</button>
              </div>
              <div class="col">
                <button class="btn btn-danger btn-block">
                    SI
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>