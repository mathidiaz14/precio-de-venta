@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('alert')
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">Calculo del precio de venta</div>
                <div class="card-body">
                   <div class="col-sm-12 ">
                        <form action="{{url('precioVenta')}}" method="post" class="form-horizontal row">
                            @csrf
                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
                            	<label for="">Producto</label>
                                <select name="producto" id="" class="form-control">
                                    @foreach(Auth::user()->productos as $producto)
                                        <option value="{{$producto->id}}">{{$producto->titulo}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-12 col-md-4 col-lg-2">
                            	<label for="">Margen de ganancia</label>
                                <input type="text" class="form-control col-sm-12" name="margen" value="" placeholder="Tu margen" required="">
                            </div>
                            <div class="form-group col-sm-12 col-md-4 col-lg-2">
                            	<label for="">Margen de tienda</label>
                                <input type="number" class="form-control col-sm-12" id="margen_tienda" name="margen_tienda" placeholder="Margen de tienda" >
                            </div>
                            <div class="form-group col-sm-12 col-md-4 col-lg-2">
                            	<label for="">Pasarela de pago %</label>
                                <input type="number" class="form-control col-sm-12" id="pasarela_pago" name="pasarela_pago" placeholder="Costo pasarela de pago">
                            </div>
                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
                            	<br>
                                <button type="submit" class="btn btn-info btn-block">
                                    <i class="fa fa-save"></i>
                                    Guardar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
		<div class="col-sm-12">
        	<br>
            <div class="card">
                <div class="card-header">Precio venta al publico</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>Producto</th>
                                <th>Mi Margen</th>
                                <th>Margen de tienda</th>
                                <th>Pasarela de pago</th>
                                <th>Precio final</th>
                                <th>Editar</th>
                                <th>Eliminar</th>
                            </tr>
                            @foreach(Auth::user()->ventas as $venta)
                                <tr>
                                    @php
                                        $precio_material = 0;
                                        foreach($venta->producto->materiales as $material)
                                            $precio_material += $material->pivot->cantidad * ($material->precio / $material->cantidad);

                                        $precio_trabajo = $venta->producto->horas_trabajo * $precio_hora_objetivo;

                                        $precio_producto = ($precio_material + $precio_trabajo + $venta->producto->paquete) * ( "1.".$venta->producto->impuestos);
                                        
                                        $precio_producto_margen = ($precio_producto * $venta->margen) / 100;

                                        $precio_producto_tienda = (($precio_producto + $precio_producto_margen) *  $venta->margen_tienda) / 100;

                                        $precio_producto_pasarela = (($precio_producto + $precio_producto_margen + $precio_producto_tienda) * $venta->pasarela_pago) / 100;
                                    @endphp
                                    
                                    <th>
                                        {{$venta->producto->titulo}} - $ {{round($precio_producto,2)}}
                                    </th>
                                    
                                    <th>{{$venta->margen}} % - ${{round($precio_producto_margen ,2)}}</th>
                                    
                                    <th>@if($venta->margen_tienda == 0) 0 @else {{$venta->margen_tienda}} @endif % - ${{round($precio_producto_tienda, 2)}}</th>
                                    
                                    <th>{{$venta->pasarela_pago}} % - ${{round($precio_producto_pasarela,2)}}</th>

                                    <th>$
                                        {{
                                            round($precio_producto + $precio_producto_margen + $precio_producto_tienda + $precio_producto_pasarela, 2)
                                        }}    

                                    </th>
                                    <th>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mensajeEditarVenta{{$venta->id}}">
                                          <i class="fa fa-edit"></i>
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="mensajeEditarVenta{{$venta->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Editar item</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                <form action="{{url('precioVenta', $venta->id)}}" method="post" class="form-horizontal">
                                                    @csrf
                                                    @method('PATCH')
                                                    <div class="form-group">
                                                        <label for="">Producto</label>
                                                        <select name="producto" id="" class="form-control">
                                                            @foreach(Auth::user()->productos as $producto)
                                                                @if($producto->id == $venta->producto_id)
                                                                    <option value="{{$producto->id}}" selected="">{{$producto->titulo}}</option>
                                                                @else
                                                                    <option value="{{$producto->id}}">{{$producto->titulo}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Margen de ganancia</label>
                                                        <input type="text" class="form-control col-sm-12" name="margen" value="{{$venta->margen}}" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Margen de tienda</label>
                                                        <input type="number" class="form-control col-sm-12" id="margen_tienda" name="margen_tienda" value="{{$venta->margen_tienda}}" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Pasarela de pago %</label>
                                                        <input type="number" class="form-control col-sm-12" id="pasarela_pago" name="pasarela_pago" value="{{$venta->pasarela_pago}}" >
                                                    </div>
                                                    <div class="form-group">
                                                        <br>
                                                        <button type="submit" class="btn btn-info btn-block">
                                                            <i class="fa fa-save"></i>
                                                            Guardar
                                                        </button>
                                                    </div>
                                                </form>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </th>
                                    <th>
                                        @include('eliminar', ['url' => url('precioVenta', $venta->id), 'id' => $venta->id])
                                    </th>
                                </tr>
                            @endforeach
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
  
@endsection