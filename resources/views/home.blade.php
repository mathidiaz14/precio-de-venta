@extends('layouts.app')

@section('content')
<div class="container-fluid">
    @include('menu')
    
    <div class="row">
        @include('alert')
        <div class="col-sm-12 col-md-4">
            <div class="card">
                <div class="card-header">Calculo del valor hora de trabajo</div>
                <div class="card-body">
                    <form action="{{url('user')}}" method="post" class="form-horizontal">
                        @csrf
                        <div class="form-group row">
                            <label for="salario_neto" class="col-md-4 col-form-label">Salario neto</label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="salario_neto" id="salario_neto" value="{{Auth::user()->salario_neto}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="horas_diarias" class="col-md-4 col-form-label">Promedio de horas diarias</label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="horas_diarias" id="horas_diarias" value="{{Auth::user()->horas_diarias}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dias_año" class="col-md-4 col-form-label">Dias de tu año laboral</label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="dias_año" id="dias_año" value="{{Auth::user()->dias_año}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fines_semana" class="col-md-4 col-form-label">Dias fin de semana</label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="fines_semana" id="fines_semana" value="{{Auth::user()->fines_semana}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="festivos" class="col-md-4 col-form-label">Dias festivos</label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="festivos" id="festivos" value="{{Auth::user()->festivos}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="vacaciones" class="col-md-4 col-form-label">Dias de vacaciones</label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="vacaciones" id="vacaciones" value="{{Auth::user()->vacaciones}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dias_baja" class="col-md-4 col-form-label">Dias de baja</label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="dias_baja" id="dias_baja" value="{{Auth::user()->dias_baja}}">
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-info">
                                <i class="fa fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-4">
            <div class="card">
                <div class="card-header">Horas no facturables menusales</div>

                <div class="card-body">

                    <div class="col-sm-12">
                        <form action="{{url('horaNoFacturada')}}" method="post" class="form-inline">
                            @csrf
                            <div class="form-group col-sm-5">
                                <input type="text" class="form-control col-sm-12" id="titulo" name="titulo" value="" placeholder="Titulo">
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="number" class="form-control col-sm-12" id="hora" name="horas" placeholder="Cantidad">
                            </div>
                            <div class="form-group col-sm-3">
                                <button type="submit" class="btn btn-info btn-block">
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <hr>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Titulo</th>
                                    <th>Horas</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                                @foreach(Auth::user()->horasNoFacturables as $hnf)
                                    <tr>
                                        <td>{{$hnf->titulo}}</td>
                                        <td>{{$hnf->horas}}</td>
                                        <td>
                                            @include('editarHora', ['hora' => $hnf])
                                        </td>
                                        <td>
                                            @include('eliminar', ['url' => url('horaNoFacturada', $hnf->id), 'id' => "hora_".$hnf->id])
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="card">
                <div class="card-header">Gastos fijos mensuales</div>

                <div class="card-body">

                    <div class="col-sm-12">
                        <form action="{{url('gastoFijo')}}" method="post" class="form-inline">
                            @csrf
                            <div class="form-group col-sm-5">
                                <input type="text" class="form-control col-sm-12" id="titulo" name="titulo" value="" placeholder="Titulo">
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="number" class="form-control col-sm-12" id="cantidad" name="cantidad" placeholder="Cantidad">
                            </div>
                            <div class="form-group col-sm-3">
                                <button type="submit" class="btn btn-info btn-block">
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <hr>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Titulo</th>
                                    <th>Cantidad</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                                @foreach(Auth::user()->gastosFijos as $gf)
                                    <tr>
                                        <td>{{$gf->titulo}}</td>
                                        <td>${{$gf->cantidad}}</td>
                                        <td>
                                            @include('editarGastosFijos', ['gasto' => $gf])
                                        </td>
                                        <td>
                                            @include('eliminar', ['url' => url('gastoFijo', $gf->id), 'id' => "gasto_".$gf->id])
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('#username').editable();
        });
    </script>
@endsection