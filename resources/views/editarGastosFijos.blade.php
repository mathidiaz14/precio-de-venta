<!-- Button trigger modal -->
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#mensajeEditarGastoFijo{{$gasto->id}}">
  <i class="fa fa-edit"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="mensajeEditarGastoFijo{{$gasto->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <form action="{{url('gastoFijo', $gasto->id)}}" method="post" class="form-inline">
	        @csrf
	        @method("PATCH")
	        <div class="form-group col-sm-5">
	            <input type="text" class="form-control col-sm-12" id="titulo" name="titulo" value="{{$gasto->titulo}}">
	        </div>
	        <div class="form-group col-sm-5">
	            <input type="number" class="form-control col-sm-12" id="cantidad" name="cantidad" value="{{$gasto->cantidad}}">
	        </div>
	        <div class="form-group col-sm-2">
	            <button type="submit" class="btn btn-info btn-block">
	                <i class="fa fa-check"></i>
	            </button>
	        </div>
	        <hr>
	    </form>
      </div>
    </div>
  </div>
</div>