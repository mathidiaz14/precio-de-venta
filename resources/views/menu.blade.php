@php
    $total_de_dias_año =   Auth::user()->dias_año 
                        - Auth::user()->fines_semana 
                        - Auth::user()->festivos 
                        - Auth::user()->vacaciones 
                        - Auth::user()->dias_baja;             

    $total_de_dias_mes = $total_de_dias_año / 12;

    $total_gastos_fijos = 0; 
    foreach (Auth::user()->gastosFijos as $gf)
        $total_gastos_fijos += $gf->cantidad;            

    $total_horas_no_facturables = 0;
    foreach(Auth::user()->horasNoFacturables as $hnf)
        $total_horas_no_facturables += $hnf->horas;

    $total_horas_no_facturables_mes = $total_horas_no_facturables * 4.33;
    
    $total_horas_facturables_mes = ($total_de_dias_mes * Auth::user()->horas_diarias) - $total_horas_no_facturables_mes;

    $costo_vacaciones = (Auth::user()->salario_neto + $total_gastos_fijos) * Auth::user()->vacaciones / 365;

    $objetivo_de_facturacion = Auth::user()->salario_neto + $total_gastos_fijos + $costo_vacaciones;

    $precio_hora_objetivo = $objetivo_de_facturacion / $total_horas_facturables_mes;
@endphp
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <p>Total de dias al año</p>
                        <p><b>{{$total_de_dias_año}} dias</b></p>
                    </div>
                    <div class="col">
                        <p>Total de dias al mes</p>
                        <p><b>{{round($total_de_dias_mes,2)}} dias</b></p>
                    </div>
                    <div class="col">
                        <p>Total horas no facturables al mes</p>
                        <p><b>{{$total_horas_no_facturables_mes}}</b></p>
                    </div>
                    <div class="col">
                        <p>Total horas facturables al mes</p>
                        <p><b>{{round($total_horas_facturables_mes,2)}} horas</b></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <p>Recuperación costo vacaciones</p>
                        <p><b>$ {{round($costo_vacaciones, 2)}}</b></p>
                    </div>
                    <div class="col">
                        <p>Objetivo facturable al mes</p>
                        <p><b>$ {{round($objetivo_de_facturacion, 2)}}</b></p>
                    </div>
                    <div class="col">
                        <p>Precio minimo de hora para cubrir costos</p>
                        <p><b>$ {{round($total_gastos_fijos/$total_horas_facturables_mes, 2)}}</b></p>
                    </div>
                    <div class="col">
                        <p>Precio de hora objetivo</p>
                        <p><b>$ {{round($precio_hora_objetivo, 2)}}</b></p>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
</div>