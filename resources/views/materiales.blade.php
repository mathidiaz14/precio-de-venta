@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('alert')
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">Materiales</div>
                <div class="card-body">
                   <div class="col-sm-12">
                        <form action="{{url('material')}}" method="post" class="form-inline">
                            @csrf
                            <div class="form-group col-sm-3">
                                <input type="text" class="form-control col-sm-12" id="titulo" name="titulo" value="" placeholder="Titulo" required="">
                            </div>
                            <div class="form-group col-sm-3">
                                <input type="number" class="form-control col-sm-12" id="cantidad" name="cantidad" placeholder="Cantidad" required="">
                            </div>
                            <div class="form-group col-sm-3">
                                <input type="number" class="form-control col-sm-12" id="precio" name="precio" placeholder="Precio" required="">
                            </div>
                            <div class="form-group col-sm-3">
                                <button type="submit" class="btn btn-info btn-block">
                                    <i class="fa fa-save"></i>
                                    Guardar
                                </button>
                            </div>
                        </form>
                    </div>
                    <hr>
                     <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Titulo</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    <th>Precio unitario</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                                @foreach(Auth::user()->materiales as $material)
                                    <tr>
                                        <td>{{$material->titulo}}</td>
                                        <td>{{$material->cantidad}}</td>
                                        <td>${{$material->precio}}</td>
                                        <td>${{$material->precio / $material->cantidad}}</td>
                                        <td>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mensajeEditarMaterial{{$material->id}}">
                                              <i class="fa fa-edit"></i>
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="mensajeEditarMaterial{{$material->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Editar item</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form action="{{url('material', $material->id)}}" method="post" class="form-horizontal">
                                                        @csrf
                                                        @method("PATCH")
                                                        <div class="form-group">
                                                            <label for="">Titulo</label>
                                                            <input type="text" class="form-control col-sm-12" id="titulo" name="titulo" value="{{$material->titulo}}" required="">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Cantidad</label>
                                                            <input type="text" class="form-control col-sm-12" id="cantidad" name="cantidad" value="{{$material->cantidad}}" required="">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Precio</label>
                                                            <input type="text" class="form-control col-sm-12" id="precio" name="precio" value="{{$material->precio}}" required="">
                                                        </div>
                                                        <hr>
                                                        <div class="form-group text-right">
                                                            <button type="submit" class="btn btn-info">
                                                                <i class="fa fa-save"></i>
                                                                Guardar
                                                            </button>
                                                        </div>
                                                    </form>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </td>
                                        <td>
                                            @include('eliminar', ['url' => url('material', $material->id), 'id' => "material_".$material->id])
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
  
@endsection