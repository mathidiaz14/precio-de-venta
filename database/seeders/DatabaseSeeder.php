<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin Admin',
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => Hash::make('123456'),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('hora_no_facturadas')->insert([
            'user_id' => '1',
            'titulo' => 'Facturación, Administración y Dirección',
            'horas' => '2',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('hora_no_facturadas')->insert([
            'user_id' => '1',
            'titulo' => 'Ventas',
            'horas' => '2',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('hora_no_facturadas')->insert([
            'user_id' => '1',
            'titulo' => 'Relaciones públicas y gestión de la publicidad',
            'horas' => '2',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('hora_no_facturadas')->insert([
            'user_id' => '1',
            'titulo' => 'Formación e I+D',
            'horas' => '2',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('hora_no_facturadas')->insert([
            'user_id' => '1',
            'titulo' => 'Descansos, tiempos muertos, desplazamientos, etc.',
            'horas' => '2',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        /***************************************************/

        DB::table('gasto_fijos')->insert([
            'user_id' => '1',
            'titulo' => 'Alquiler',
            'cantidad' => '0',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('gasto_fijos')->insert([
            'user_id' => '1',
            'titulo' => 'Luz',
            'cantidad' => '0',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('gasto_fijos')->insert([
            'user_id' => '1',
            'titulo' => 'Papeleria',
            'cantidad' => '0',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('gasto_fijos')->insert([
            'user_id' => '1',
            'titulo' => 'Telefono',
            'cantidad' => '0',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('gasto_fijos')->insert([
            'user_id' => '1',
            'titulo' => 'Publicidad',
            'cantidad' => '0',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('gasto_fijos')->insert([
            'user_id' => '1',
            'titulo' => 'Impuestos',
            'cantidad' => '0',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('gasto_fijos')->insert([
            'user_id' => '1',
            'titulo' => 'Otros',
            'cantidad' => '0',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
