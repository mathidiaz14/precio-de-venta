<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrecioVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precio_ventas', function (Blueprint $table) {
            $table->id();
            $table->string('producto_id');
            $table->string('margen')->nullable();
            $table->string('margen_tienda')->nullable();
            $table->string('pasarela_pago')->nullable();
            $table->string('otro')->nullable();
            $table->string('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precio_ventas');
    }
}
