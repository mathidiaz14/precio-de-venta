<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MaterialControlador;
use App\Http\Controllers\ProductoControlador;
use App\Http\Controllers\GastoFijoControlador;
use App\Http\Controllers\HoraNoFacturadaControlador;
use App\Http\Controllers\PrecioVentaControlador;
use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('login');
});

Route::get('home', [HomeController::class, 'index'])->name('home');

Route::resource('material'          , MaterialControlador::class)->middleware('auth');
Route::resource('producto'          , ProductoControlador::class)->middleware('auth');
Route::resource('gastoFijo'         , GastoFijoControlador::class)->middleware('auth');
Route::resource('horaNoFacturada'   , HoraNoFacturadaControlador::class)->middleware('auth');
Route::resource('precioVenta'       , PrecioVentaControlador::class)->middleware('auth');
Route::resource('user'              , UserController::class)->middleware('auth');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
